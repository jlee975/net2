#include "protocol.h"

#include <stdexcept>

#include <unistd.h>

std::vector< std::byte > serialize(const hello_message& m)
{
	std::vector< std::byte > v;

	for (std::size_t i = 0; i < m.text.length(); ++i)
	{
		v.push_back( static_cast< std::byte >( static_cast< unsigned char >( m.text[i] ) ) );
	}

	return v;
}

namespace details
{
void send(
	int                             fd,
	const std::vector< std::byte >& x
)
{
	const std::size_t total_length = x.size() + 1;

	if ( total_length >= 256 )
	{
		throw std::logic_error("Not implemented");
	}

	const unsigned char buf[] = { static_cast< unsigned char >( total_length ),
		                          static_cast< unsigned char >( hello_id ) };

	::write(fd, buf, 2);
	::write( fd, x.data(), x.size() );
}

}

bool deserialize(
	hello_message&   m,
	const std::byte* p_,
	std::size_t      n
)
{
	const auto* p = reinterpret_cast< const char* >( p_ );

	m.text.assign(p, p + n);
	return true;
}
