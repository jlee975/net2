#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <string>
#include <vector>
#include <cstddef>

#include "net/port_type.h"

enum messages : unsigned char {hello_id};

template< messages M >
struct message_contents;

template<>
struct message_contents< hello_id >
{
	std::string text;
};

using hello_message = message_contents< hello_id >;

std::vector< std::byte > serialize(const hello_message&);
bool deserialize(hello_message&, const std::byte*, std::size_t);

namespace details
{
void send(int, const std::vector< std::byte >&);
}

template< messages M >
void send(
	int                          fd,
	const message_contents< M >& m
)
{
	details::send( fd, serialize(m) );
}

const jl::net::iana::port default_port(static_cast< jl::net::iana::port >( 3120 ) );

#endif // PROTOCOL_H
