#include <iostream>
#include <thread>

#include "event/instance.h"
#include "net/server.h"
#include "net/bufferedconnection.h"
#include "protocol.h"

class MyServer
	: public jl::Server
{
public:
	using Server::Server;

	bool is_finished() const
	{
		return finished;
	}
private:
	class MyConnection
		: public jl::BufferedConnection
	{
public:
		MyConnection(
			MyServer* server_,
			int       fd_
		)
			: BufferedConnection(fd_), server(server_)
		{
// std::cout << "Connection created " << fd << std::endl;
		}

		~MyConnection()
		{
			/*
			   char buf[1024];
			   if (::read(fd, buf, 1024) > 0)
			    std::cout << "huh... was able to read from " << fd << std::endl;
			 */
// std::cout << "Connection destroyed " << fd << std::endl;
		}

		/// @todo Implement
		std::size_t data_received(
			const std::byte* buf,
			std::size_t      n
		) final
		{
			std::size_t i = 0;

			while ( i < n )
			{
				// Read length of packet
				const std::size_t len = static_cast< unsigned char >( buf[i] );

				if ( i + len < n )
				{
					// read type of message
					switch ( static_cast< unsigned char >( buf[i + 1] ) )
					{
					case messages::hello_id:
						// deserialize message and handle
						hello_message m;
						/// @todo Deserialize to a "view" so we don't have to allocate hello_message::text
						deserialize(m, buf + i + 2, len - 1);
						std::cout << m.text << std::endl;

						break;
					}

					i += len + 1;
				}
				else
				{
					break;
				}
			}

			return i;
		}
private:
		MyServer* server;
	};

	MyConnection* create_connection(int fd_) override
	{
		return new MyConnection(this, fd_);
	}

	bool finished = false;
};

int main()
{
	MyServer server(default_port);

	// start the default_application_eventloop
	if ( jl::default_application_eventloop().subscribe(&server) )
	{
		server.listen();
	}

	/// @todo while (server.is_working()) { }
	while ( !server.is_finished() )
	{
		std::this_thread::sleep_for( std::chrono::milliseconds(100) );
	}

	// install a server that accepts connections

	jl::default_application_eventloop().stop();
	jl::default_application_eventloop().wait();

	return 0;
}
