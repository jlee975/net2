/* Have eventloop wait for pid to close
 */
#include <cstdlib>
#include <iostream>
#include <thread>

#include <unistd.h>

#include "event/instance.h"
#include "event/eventsources/pidevent.h"

class PIDWatcher
	: public jl::ProcessEvent
{
public:
	using ProcessEvent::ProcessEvent;
private:
	void application_waited_on(jl::EventControl& control) override
	{
		std::cout << "Application stopped" << std::endl;
		std::cout << "Stopping Event Loop" << std::endl;
		control.stop();
	}

};

int main()
{
	const auto pid = ::fork();

	// fork. In the parent wait for the child to signal the event fd
	switch ( pid )
	{
	case -1:
		// Error
		return EXIT_FAILURE;

	case 0:
		// Child -- sleep a bit then exit
		std::this_thread::sleep_for(std::chrono::seconds(4) );
		return EXIT_SUCCESS;

	default:
		break;
	}

	/// @bug This is deleted by epoll because it reports finished. Awkward.
	auto watcher = new PIDWatcher(pid);

	jl::default_application_eventloop().subscribe(watcher);

	jl::default_application_eventloop().wait();

	return EXIT_SUCCESS;
}
