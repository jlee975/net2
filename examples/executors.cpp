#include <iostream>
#include <thread>

#include "event/instance.h"

void doubler(std::string s)
{
	std::cout << s << s << std::endl;
}

class Prefixer
{
public:
	explicit Prefixer(std::string prefix_)
		: prefix(std::move(prefix_) )
	{
	}

	void operator()(int x) const
	{
		std::cout << prefix << x << "!" << std::endl;
	}

	void operator()(std::string s) const
	{
		std::cout << prefix << s << std::endl;
	}
private:
	std::string prefix;
};

class Quoter
{
public:
	explicit Quoter(std::string s_)
		: s(std::move(s_) )
	{
	}

	void print() const
	{
		std::cout << '"' << s << '"' << std::endl;
	}
private:
	std::string s;
};

void example1()
{
	auto executor = jl::create_executor(doubler);

	for (int i = 0; i < 10; ++i)
	{
		executor(std::to_string(i) );
	}
}

void example2()
{
	auto executor = jl::create_executor([](std::string s){ std::cout << s << std::endl; });

	for (int i = 0; i < 10; ++i)
	{
		executor(std::to_string(i) );
	}
}

void example3()
{
	auto executor = jl::create_executor(Prefixer("#") );

	for (int i = 0; i < 10; ++i)
	{
		executor(std::to_string(i) );
		executor(i);
	}
}

void example4()
{
	auto executor = jl::create_executor(&Quoter::print);

	/// @todo Would be nice if we didn't have to specify Quoter. For a pointer-to-member-function, first
	/// argument can be used to construct the object
	for (int i = 0; i < 10; ++i)
	{
		executor(Quoter(std::to_string(i) ) );
	}
}

int main()
{
	example1();
	example2();
	example3();
	example4();

	// Sleep, becuase if we stop the threads too soon then some queued events will not get
	// executed
	std::this_thread::sleep_for(std::chrono::milliseconds(500) );

	/// @todo This is awkward
	jl::default_application_eventloop().stop();
	jl::default_application_eventloop().wait();

	std::cout << "STOPPING WORKED" << std::endl;

	return 0;
}
