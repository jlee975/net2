add_executable(executors executors.cpp)
target_link_libraries(executors jlee)

add_executable(clocks clocks.cpp)
target_link_libraries(clocks jlee)

add_executable(fork forkcomm.cpp)
target_link_libraries(fork jlee)

add_executable(pidfd pidfd.cpp)
target_link_libraries(pidfd jlee)
