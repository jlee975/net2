#include <thread>
#include <iostream>

#include "event/instance.h"
#include "event/eventsources/timerevent.h"

class TimerExample
	: public jl::MonotonicTimerEvent
{
	void triggered() override
	{
		std::cout << "Timer event occurred" << std::endl;
	}

};

void example1()
{
	TimerExample ev;

	/// @todo Should probably be oneshot, and re-armed
	jl::default_application_eventloop().subscribe(&ev);

	ev.start(1, 0);

	std::this_thread::sleep_for(std::chrono::milliseconds(10500) );
	ev.stop();
	jl::default_application_eventloop().stop();
	jl::default_application_eventloop().wait();
}

int main()
{
	example1();
}
