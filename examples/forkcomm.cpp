/* Signal a parent's event loop from a child process
 */
#include <cstdlib>
#include <iostream>
#include <unistd.h>

#include "event/notificationsource.h"
#include "event/instance.h"


class ParentNotification
	: public jl::NotificationSource
{
public:
	ParentNotification()
		: jl::NotificationSource(jl::EventSource::normal, false)
	{
	}

	void triggered(jl::EventControl& control) override
	{
		std::cout << "Got message from child process" << std::endl;
		control.stop();
	}

};

int main()
{
	// create an eventfd that can be copied across processes
	ParentNotification link;

	// fork. In the parent wait for the child to signal the event fd
	switch ( ::fork() )
	{
	case -1:
		// Error
		return EXIT_FAILURE;

	case 0:
		// Child
		link.notify();
		return EXIT_SUCCESS;

	default:
		break;
	}

	jl::default_application_eventloop().subscribe(&link);

	std::cout << "Stopping" << std::endl;

	jl::default_application_eventloop().wait();

	return EXIT_SUCCESS;
}
