#include <thread>
#include <iostream>

#include "thread/concurrent_queue.h"

int main()
{
	const int nthreads = 10;
	const int ninserts = 10000;

	concurrent_queue< int > q;

	for (int i = 0; i < nthreads; ++i)
	{
		std::thread t([&q](){
		              for (int j = 0; j < ninserts; ++j)
					  {
						  q.emplace_back(j);
					  }
			});
		t.detach();
	}

	std::cout << "Threads started" << std::endl;
	int total               = 0;
	int subtotals[ninserts] = {};

	while ( total < nthreads * ninserts )
	{
		if ( auto p = q.pop_front() )
		{
			subtotals[*p] += 1;
			++total;
		}
	}

	std::cout << "Done iterating" << std::endl;

	if ( q.pop_front() )
	{
		throw std::logic_error("Bad implementation");
	}

	for (int i = 0; i < ninserts; ++i)
	{
		if ( subtotals[i] != nthreads )
		{
			throw std::logic_error("Incorrect count");
		}
	}

	std::cout << "Everything OK" << std::endl;
}
