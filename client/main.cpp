/** @file
 *
 *  This is a simple sockets-style client that sends the PID to a server 100 times
 */
#include <iostream>
#include <thread>

#include <unistd.h>

#include <sys/socket.h>
#include <arpa/inet.h>

#include "net/port_type.h"

#include "protocol.h"

int main(
	int   argc,
	char* argv[]
)
{
	const char* remote = "::1";

	sockaddr_in6 server_address = {};

	server_address.sin6_family = AF_INET6;
	server_address.sin6_port   = htons(to_port_type(default_port) );

	if ( inet_pton(AF_INET6, remote, &server_address.sin6_addr) == 1 )
	{
		const int fd = ::socket(AF_INET6, SOCK_STREAM | SOCK_CLOEXEC, IPPROTO_TCP);

		if ( fd != -1 )
		{
			const int result =
				::connect(fd, reinterpret_cast< sockaddr* >( &server_address ), sizeof( server_address ) );

			if ( result == 0 )
			{
				hello_message m = { .text = std::to_string(::getpid() ) };

				if ( argc >= 2 )
				{
					m.text = argv[1];
				}

				for (int i = 0; i < 100; ++i)
				{
					send(fd, m);
				}

				std::this_thread::sleep_for(std::chrono::seconds(4) );

				::close(fd);
			}
		}
	}

	return 0;
}
