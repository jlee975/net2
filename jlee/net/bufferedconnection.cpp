#include "bufferedconnection.h"

#include <iostream>

#include <unistd.h>
#include <sys/socket.h>

namespace jl
{
BufferedConnection::BufferedConnection(int fd_)
	: Connection(fd_)
{

}

std::size_t BufferedConnection::data_received(
	const std::byte*,
	std::size_t n
)
{
	return n;
}

void BufferedConnection::data_available(
	const std::byte* in,
	std::size_t      n
)
{
	/// @todo Avoid the copy/alloc where possible
	if ( read_buffer.empty() )
	{
		const std::size_t consumed = data_received(in, n);

		read_buffer.insert(read_buffer.end(), in + consumed, in + n);
	}
	else
	{
		read_buffer.insert(read_buffer.end(), in, in + n);

		const std::size_t consumed = data_received(read_buffer.data(), read_buffer.size() );

		read_buffer.erase(read_buffer.begin(), read_buffer.begin() + consumed);
	}
}

/// @todo Write and buffer
void BufferedConnection::send(
	const std::byte* p,
	std::size_t      n
)
{
	std::scoped_lock lk(write_mut);

	if ( write_buffer.empty() )
	{
		const auto total_written = ready_write_locked(p, n);

		write_buffer.insert(write_buffer.end(), p + total_written, p + n);
	}
	else
	{
		write_buffer.insert(write_buffer.end(), p, p + n);

		const auto total_written = ready_write_locked(write_buffer.data(), write_buffer.size() );

		write_buffer.erase(write_buffer.begin(), write_buffer.begin() + total_written);
	}
}

void BufferedConnection::ready_write()
{
	std::scoped_lock lk(write_mut);

	const auto total_written = ready_write_locked(write_buffer.data(), write_buffer.size() );

	write_buffer.erase(write_buffer.begin(), write_buffer.begin() + total_written);
}

std::size_t BufferedConnection::ready_write_locked(
	const std::byte* p,
	std::size_t      total_available
)
{
	std::size_t total_written = 0;

	while ( total_written < total_available )
	{
		const auto res = ::send(fd, p + total_written, total_available - total_written, MSG_NOSIGNAL);

		if ( res == -1 )
		{
			if ( errno == EAGAIN || errno == EWOULDBLOCK )
			{
				set_wait_for_write(true);
			}

			/// @todo Check for something other than EAGAIN
			/// @todo Handle EPIPE
			break;
		}
		else
		{
			total_written += static_cast< std::size_t >( res );
		}
	}

	return total_written;
}

}
