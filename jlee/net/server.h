#ifndef JLEE_NET_SERVER_H
#define JLEE_NET_SERVER_H

#include "../event/eventsource.h"

#include "port_type.h"

namespace jl
{
class Connection;

/// @todo Rename to TCPServer
class Server
	: public EventSource
{
public:
	explicit Server(jl::net::iana::port port_) noexcept;

	bool listen();
private:
	result handle_event(event_type e, EventControl&) final;

	explicit Server(int, jl::net::iana::port);

	static int create_server_socket(jl::net::iana::port port_);

	void incoming_connection(EventControl&);

	bool add_connection(int, EventControl&);

	/// Implementers must take ownership of the file descriptor given (if the return is not null)
	virtual Connection* create_connection(int) = 0;

	const jl::net::port_type port;
};
}

#endif // JLEE_NET_SERVER_H
