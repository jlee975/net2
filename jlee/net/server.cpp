#include "server.h"

#include <iostream>
#include <cerrno>
#include <unistd.h>

#include <sys/socket.h>
#include <arpa/inet.h>

#include "../event/eventloop.h"

#include "connection.h"
#include "utility.h"

namespace jl
{
Server::Server(jl::net::iana::port port_) noexcept
	: Server(create_server_socket(port_), port_)
{
}

Server::Server(
	int                 fd_,
	jl::net::iana::port port_
)
	: EventSource(EventSource::normal, fd_, jl::event_type::read | jl::event_type::edge_triggered),
	port(port_ != jl::net::iana::port::random ? to_port_type(port_) : get_port_from_socket(fd_) ) // Get the actual port bound on the file descriptor
{

}

/// @todo Rename to "start" or something
bool Server::listen()
{
	return ::listen(fd, SOMAXCONN) == 0;
}

Server::result Server::handle_event(
	event_type    e,
	EventControl& el
)
{
	// An event on the server socket. Typically an incoming connection
	if ( (e& event_type::error) != event_type::none || (e& event_type::hangup) != event_type::none )
	{
		// Error on the server socket. It's about to hit the fan.
		/// @todo Need a way to indicate failure
		return result::reenable;
	}

	if ( (e& event_type::read) != event_type::none )
	{
		// Handle the incoming connection
		/// @bug Do we have to reenable the server socket
		incoming_connection(el);
	}

	return result::reenable;
}

int Server::create_server_socket(jl::net::iana::port port_)
{
	/// @todo If ipv6 fails, can we fall back to ipv4?
	const int fd = ::socket(AF_INET6, SOCK_STREAM | SOCK_CLOEXEC | SOCK_NONBLOCK, IPPROTO_TCP);

	if ( fd != -1 )
	{
		{
			int optval = 1;
			setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &optval, sizeof( optval ) );
		}

		sockaddr_in6 addr = {};

		addr.sin6_family = AF_INET6;
		addr.sin6_port   = htons(to_port_type(port_) );
		addr.sin6_addr   = in6addr_any;

		// Bind the socket to an address
		if ( ::bind(fd, reinterpret_cast< sockaddr* >( &addr ), sizeof( addr ) ) == 0 )
		{
			return fd;
		}

		/// @todo Respond to some of the more common errors
		::close(fd);
	}

	return -1;
}

void Server::incoming_connection(EventControl& el)
{
	while ( true )
	{
		/// @todo If the socket can't be done non-blocking, add it as such
		/// @todo Get and save the client address
		const int infd = accept4(fd, nullptr, nullptr, SOCK_NONBLOCK | SOCK_CLOEXEC);

		if ( infd >= 0 )
		{
			// Connection from peer was accepted
			if ( !add_connection(infd, el) )
			{
				// Some kind of failure
				::close(infd);
			}
		}
		else if ( errno == EAGAIN || errno == EWOULDBLOCK )
		{
			// No more connections available. We're done for now.
			return;
		}
		else
		{
			/// @todo Unrecoverable errors should cause the socket to close
			/// @bug Can probably still continue accepting connections after ECONNABORTED, EINTR, EPERM, and other
			/// errors
			/// @bug According to man, the error may be from the new socket, not the server socket. Should call accept
			/// again in certain conditions (again, see man accept -> RETURN VALUE -> Error handling)
			return;
		}
	}
}

bool Server::add_connection(
	int           fd_,
	EventControl& el
)
{
	/// @bug Delete at the approrpriate time
	if ( Connection* cxn = create_connection(fd_) )
	{
		if ( el.subscribe(cxn) )
		{
			return true;
		}

		delete cxn;
		return false;
	}

	// fd_ is lost
	::close(fd_);

	return false;
}

}
