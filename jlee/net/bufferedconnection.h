#ifndef BUFFEREDCONNECTION_H
#define BUFFEREDCONNECTION_H

#include <vector>
#include <cstddef>
#include <mutex>

#include "connection.h"

namespace jl
{
/// @todo Shared allocator from server, so memory can be recycled
class BufferedConnection
	: public Connection
{
public:
	explicit BufferedConnection(int);
protected:
	void send(const std::byte*, std::size_t);
private:
	/// @todo I don't think there's a real need to ensure "in" is const
	void data_available(const std::byte*, std::size_t) final;

	virtual std::size_t data_received(const std::byte*, std::size_t);

	void ready_write() final;

	std::size_t ready_write_locked(const std::byte*, std::size_t);

	/// @todo A circular buffer or something would be better
	std::vector< std::byte > read_buffer;

	/// @todo Write should only occur from one thread
	/// @todo Shared mutex. Sending is read-only
	std::mutex               write_mut;
	std::vector< std::byte > write_buffer;
};
}
#endif // BUFFEREDCONNECTION_H
