#ifndef JLEE_NET_CONNECTION_H
#define JLEE_NET_CONNECTION_H

#include <cstddef>

#include "../event/eventsource.h"

namespace jl
{
/// @todo Could probably abstract to a "stream" class and moved to event module
class Connection
	: public EventSource
{
public:
	explicit Connection(int fd_);
private:
	result handle_event(event_type x, EventControl&) override;

	virtual void data_available(
		const std::byte*,
		std::size_t
	)
	{
	}

	virtual void ready_write()
	{

	}

};
}

#endif // JLEE_NET_CONNECTION_H
