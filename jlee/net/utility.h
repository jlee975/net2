#ifndef JLEE_NET_UTILITY_H
#define JLEE_NET_UTILITY_H

#include "port_type.h"

jl::net::port_type get_port_from_socket(int);

#endif // JLEE_NET_UTILITY_H
