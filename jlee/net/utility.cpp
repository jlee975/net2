#include "utility.h"

#include <cerrno>

#include <sys/socket.h>
#include <arpa/inet.h>

jl::net::port_type get_port_from_socket(int fd)
{
	sockaddr_in6 server;
	socklen_t    len = sizeof( server );

	if ( ::getsockname(fd, reinterpret_cast< sockaddr* >( &server ), &len) == 0 )
	{
		return ntohs(server.sin6_port);
	}

	return 0;
}
