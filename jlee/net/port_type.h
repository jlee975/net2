#ifndef JLEE_NET_PORT_TYPE_H
#define JLEE_NET_PORT_TYPE_H

#include <cstdint>

namespace jl
{
namespace net
{
/// Value holding a port, in host order
using port_type = std::uint16_t;

namespace iana
{
/// @todo A complete list can be found on iana.org
enum class port : port_type
{
	random = 0,
	ftp = 21,
	http = 80
};

constexpr port_type to_port_type(port p)
{
	return static_cast< port_type >( p );
}

}
}
}
#endif // JLEE_NET_PORT_TYPE_H
