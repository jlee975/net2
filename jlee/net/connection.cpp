#include "connection.h"

#include <iostream>
#include <cerrno>

#include <unistd.h>

namespace jl
{
Connection::Connection(int fd_)
	: EventSource(EventSource::oneshot, fd_, event_type::read | event_type::write | event_type::rdhup | event_type::edge_triggered)
{
}

/// @todo This same logic should work for reading a regular file
Connection::result Connection::handle_event(
	event_type x,
	EventControl&
)
{
	/// @todo Write should really happen first, since a read can cause more writes to happen. It would
	/// minimize the amount of buffered data (if there is any) and we could potentially clear the ready to write flag
	result ok = result::reenable;

	if ( (x& event_type::read) != event_type::none )
	{
		unsigned char buf[1000];

		while ( true )
		{
			const ssize_t n = ::read(fd, buf, 1000);

			/// @todo Data should be transferable to derived class, and buffer returned
			if ( n == -1 )
			{
				if ( errno == EAGAIN || errno == EWOULDBLOCK )
				{
					break;
				}

				/// @todo Handle some sort of error
			}
			else if ( n == 0 )
			{
				/// @todo Handle end of stream
				ok = result::finished;
				break;
			}
			else
			{
				// n > 0
				data_available(reinterpret_cast< std::byte* >( buf ), n);
			}
		}
	}

	if ( (x& event_type::rdhup) != event_type::none )
	{
		ok = result::finished;
	}
	else if ( (x& event_type::write) != event_type::none )
	{
		ready_write();
	}

	return ok;
}

}
