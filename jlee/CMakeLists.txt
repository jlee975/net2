file(GLOB_RECURSE PROJECT_SOURCES *.cpp *.h)

add_library(jlee STATIC ${PROJECT_SOURCES})
target_include_directories(jlee INTERFACE .)
