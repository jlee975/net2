#ifndef JLEE_CONCURRENT_QUEUE_H
#define JLEE_CONCURRENT_QUEUE_H

#include <mutex>

/// @todo Lock free
template< typename T >
class concurrent_queue
{
	struct node
	{
		T value;
		node* next;
	};
public:
	class node_handle
	{
public:
		node_handle()
			: p(nullptr)
		{
		}

		explicit node_handle(node* p_)
			: p(p_)
		{
		}

		~node_handle()
		{
			delete p;
		}

		node_handle(const node_handle&) = delete;
		node_handle(node_handle&& o)
			: p(o.p)
		{
			o.p = nullptr;
		}

		node_handle& operator=(const node_handle&) = delete;
		node_handle& operator=(node_handle&& o)
		{
			std::swap(p, o.p);
			return *this;
		}

		explicit operator bool()
		{
			return p;
		}
		T& operator*() const
		{
			return p->value;
		}
private:
		node* p;
	};

	template< typename...Args >
	void emplace_back(Args&&... args)
	{
		// Construct the list element without holding the lock
		node* p = new node{ .value{ std::forward< Args >(args)... }, .next{ nullptr }};

		// Only hold the lock while we append the single item
		std::scoped_lock lk(mut);

		if ( tail )
		{
			tail->next = p;
		}
		else
		{
			head = p;
		}

		tail = p;
	}

	node_handle pop_front()
	{
		if ( std::scoped_lock lk(mut);head )
		{
			node_handle t(head);

			head = head->next;

			if ( !head )
			{
				tail = nullptr;
			}

			return t;
		}

		return {};
	}
private:
	mutable std::mutex mut;
	node*              head = nullptr;
	node*              tail = nullptr;
};

#endif // JLEE_CONCURRENT_QUEUE_H
