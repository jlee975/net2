#ifndef JLEE_EVENT_EVENTSOURCE_H
#define JLEE_EVENT_EVENTSOURCE_H

#include "noncopyable.h"
#include "event_type.h"

namespace jl
{
class EventLoop;
class EventControl;

/** An object that produces and handles events
 *
 * @todo Observable, so EventLoop can know if object has been destroyed, also FunctionEvent::Executor-s, etc.
 */
class EventSource
	: NonCopyable
{
	friend class EventLoop;
	friend class EventControl;
protected:
	/// @todo At the moment, only need shutdown and not-shutdown (normal). Shutdown is an EventSource
	/// internally made by EventLoop. It may have a better way to handle that.
	enum hint
	{
		normal,
		oneshot
	};

	enum class result
	{
		reenable, ///< Schedule this EventSource again, to receive more events
		finished  ///< Object is done and does not need to be rescheduled
	};

	/// @todo The hint is always normal, except for the shutdown event created internally be EventLoop
	EventSource(hint, int, event_type) noexcept;

	virtual ~EventSource() noexcept;

	virtual result handle_event(event_type, EventControl&) = 0;

	bool set_wait_for_write(bool);

	virtual void about_to_be_deleted()
	{
	}

	const int fd;
private:
	const event_type events_to_monitor;
	const hint       event_hint;

	/// @todo This does not belong here. The meaning of this should be returned by handle_event
	bool wait_for_write;
};
}

#endif // JLEE_EVENT_EVENTSOURCE_H
