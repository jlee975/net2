#ifndef JLEE_EVENT_FUNCTIONEVENT_H
#define JLEE_EVENT_FUNCTIONEVENT_H

#include <functional>
#include "../notificationsource.h"
#include "../../thread/concurrent_queue.h"

namespace jl
{
/// @todo I think we only ever need one of these (per EventLoop, anyway)
/// @todo Rename something like ExecutorSource
/// @todo Extract as a signals and slots module
class FunctionEvent
	: public NotificationSource
{
public:
	/// @todo An executor that can only execute once, and thus be able to move f
	/// @todo schedule_with_delay_and_or_repeat
	template< typename F >
	class Executor
	{
public:
		Executor(
			F              f_,
			FunctionEvent* e_
		)
			: e(e_), f( std::move(f_) )
		{
			if ( e == nullptr )
			{
				throw std::invalid_argument("Must have access to a FunctionEvent object");
			}
		}

		/// @todo future for result of calling f
		template< typename...Args >
		void operator()(Args&&... args) const
		{
			/** @todo May want std::packaged_task here
			 * @todo e->queue(std::make_unique< FunctionEventCompleter< F, std::decay_t< Args >... > >(f, std::forward< Args >(args)...));
			 * @todo Ensure this avoids unnecessary copies and exactly follows the way std::thread(f, args...) would run
			 * @todo Avoid copying f for certain types (ex., regular functions)
			 * @todo thread specifies that f_copy be moved, but might not need it here, and it could be preferable to keep it (ex., accumulator)
			 * @todo if constexpr (sizeof...(Args)==0)
			 * @todo if f is a pointer to member function, and not invokable with the args, but the ptmf class is constructable from the first
			 *  arg and is *then* invokable... do that. Possibly more trouble if T can be constructed with more arguments. Piecewise construction?
			 *  Actually, we'll know how many arguments will be needed to send to the ptmf, so the remaining could be used for the constructor
			 *  Or just handle the special case of no args, so all args are constructor arguments
			 */
			e->list.emplace_back([f_copy = f, ... args_copy = std::decay_t< Args >( std::forward< Args >(args) )](){ std::invoke(std::move(f_copy), std::move(args_copy)...); });
			e->notify();
		}
private:
		FunctionEvent* e;
		F              f;
	};

	FunctionEvent();

	template< typename F >
	auto create_executor(F&& f)
	{
		return Executor< std::decay_t< F > >(std::forward< F >(f), this);
	}
private:
	void triggered(EventControl& control) override;

	/// @todo the type of function stored here should be noexcept
	/// @todo std::function is costly. If this were more closely tied to concurrent_queue there could be savings.
	using FunctionList = concurrent_queue< std::function< void ( ) > >;

	FunctionList list;
};
}

#endif // JLEE_EVENT_FUNCTIONEVENT_H
