#include "../eventsource.h"

namespace jl
{
class ProcessEvent
	: public EventSource
{
public:
	explicit ProcessEvent(int pid_);

	result handle_event(jl::event_type, EventControl&) final;
private:
	virtual void application_finished(EventControl& control);
	virtual void application_waited_on(EventControl& control);

	const int pid;
};
}
