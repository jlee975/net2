#include "pidevent.h"

#include <cstdlib>

#include <sys/syscall.h>
#include <sys/wait.h>

namespace jl
{
/// @todo Check the result of pidfd_open
/// @todo PIDFD_NONBLOCK
/// @todo send a signal with pidfd_send_signal
ProcessEvent::ProcessEvent(int pid_)
	: EventSource(EventSource::oneshot, syscall(SYS_pidfd_open, pid_, 0), jl::event_type::read | jl::event_type::edge_triggered),
	pid(pid_)
{
}

EventSource::result ProcessEvent::handle_event(
	event_type,
	EventControl& control
)
{
	application_finished(control);

	int status = EXIT_FAILURE;

	::waitpid(pid, &status, 0);

	/// @todo Pass result of waitpid to application_waited_onq
	application_waited_on(control);

	return result::finished;
}

void ProcessEvent::application_finished(EventControl& control)
{
}

void ProcessEvent::application_waited_on(EventControl& control)
{
}

}
