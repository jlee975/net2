#include "timerevent.h"

#include <iostream>

#include <unistd.h>
#include <sys/timerfd.h>

namespace jl
{
TimerEvent::TimerEvent(int clock)
	: EventSource(EventSource::oneshot, timerfd_create(clock, TFD_NONBLOCK | TFD_CLOEXEC), jl::event_type::read | jl::event_type::edge_triggered)
{
}

EventSource::result TimerEvent::handle_event(
	event_type,
	EventControl&
)
{
	char buf[8];

	while ( true )
	{
		// Read until EAGAIN, because of edge triggered (should be next read)
		const ssize_t n = ::read(fd, buf, 8);

		if ( n == -1 )
		{
			// EAGAIN is expected
			if ( errno == EAGAIN || errno == EWOULDBLOCK )
			{
				break;
			}

			// Some other error occurred.
			/// @todo Handle some sort of error
			stop();
			return result::finished;
		}

		triggered();
	}

	return result::reenable;
}

void TimerEvent::start(
	time_t sec,
	long   nsec
)
{
	start(sec, nsec, sec, nsec);
}

void TimerEvent::start(
	time_t isec,
	long   insec,
	time_t dsec,
	long   dnsec
)
{
	itimerspec ts = {{ isec, insec }, { dsec, dnsec }};

	timerfd_settime(fd, 0, &ts, nullptr);
}

void TimerEvent::stop()
{
	const itimerspec ts = {{ 0, 0 }, { 0, 0 }};

	timerfd_settime(fd, 0, &ts, nullptr);
}

MonotonicTimerEvent::MonotonicTimerEvent()
	: TimerEvent(CLOCK_MONOTONIC)
{

}

}
