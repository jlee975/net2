#ifndef JLEE_TIMEREVENT_H
#define JLEE_TIMEREVENT_H

#include <ctime>
#include "../eventsource.h"

namespace jl
{
class TimerEvent
	: public EventSource
{
public:
	/// @todo enum for different types of clocks
	explicit TimerEvent(int clock);

	/// @todo std chrono durations
	void start(time_t, long);
	void start(time_t, long, time_t, long);
	void stop();
private:

	result handle_event(event_type, EventControl&) override;
	virtual void triggered()
	{
	}

};

class MonotonicTimerEvent
	: public TimerEvent
{
public:
	MonotonicTimerEvent();

};
}

#endif // ifndef JLEE_TIMEREVENT_H
