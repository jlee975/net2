#include "functionevent.h"

namespace jl
{
FunctionEvent::FunctionEvent()
	: NotificationSource(EventSource::normal)
{
}

void FunctionEvent::triggered(EventControl& control)
{
	/// @todo Can we eliminate a level of indirection. Already here via Event::run
	/// can probably have the EventLoop switch on type and call run_inner directly
	if ( auto pf = list.pop_front() )
	{
		/// @todo The function, args, and results should be available in the future
		( *pf ) ();
	}
}

}
