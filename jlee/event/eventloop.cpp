#include "eventloop.h"

#include <thread>
#include <mutex>

#include <unistd.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>

#include "eventsource.h"

namespace jl
{
EventControl::EventControl(
	int efd_,
	int shutdown_fd_
)
	: efd(efd_), shutdown_fd(shutdown_fd_)
{

}

void EventControl::stop() noexcept
{
	// Try a graceful shutdown by signalling a special event
	/// @todo Is there a way to ensure the shutdown event is read first? Before other events?
	/// Would probably need to loop over all EventSource objects and cancel them.
	::eventfd_write(shutdown_fd, UINT64_C(0xfffffffffffffffe) );
}

bool EventControl::subscribe(EventSource* e) noexcept
{
	if ( e )
	{
		epoll_event event = { .events = static_cast< std::uint32_t >( e->events_to_monitor ),
			                  .data = { .ptr = e }};

		if ( e->event_hint == EventSource::oneshot )
		{
			event.events |= EPOLLONESHOT;
		}

		if ( ::epoll_ctl(efd, EPOLL_CTL_ADD, e->fd, &event) == 0 )
		{
			return true;
		}
	}

	return false;
}

EventLoop::~EventLoop()
{
	if ( thread_counter.load() != 0 )
	{
		// Threads are still running. This is like calling ~thread without join-ing
		std::terminate();
	}
}

bool EventLoop::start() noexcept
{
	std::scoped_lock lk(mut);

	if ( init() )
	{
		const std::uint_fast32_t count = 8;

		/// @todo memory_order_relaxed
		thread_counter.fetch_add(count);

		for (std::uint_fast32_t i = 0; i < count; ++i)
		{
			std::thread t(&EventLoop::epoll_thread_wrapper, this);
			t.detach();
		}

		return true;
	}

	return false;

}

bool EventLoop::init() noexcept
{
	// Open up epoll instance if don't already have one
	if ( efd == -1 )
	{
		if ( const int fd = ::epoll_create1(0);fd != -1 )
		{
			// Create the shutdown event so we can signal threads to stop
			if ( const int shfd = ::eventfd(0, EFD_NONBLOCK | EFD_SEMAPHORE);shfd != -1 )
			{
				epoll_event event = { .events = EPOLLIN,
					                  .data = { .ptr = nullptr }};

				// Ensure we can listen for the shutdown event
				if ( ::epoll_ctl(fd, EPOLL_CTL_ADD, shfd, &event) == 0 )
				{
					efd         = fd;
					shutdown_fd = shfd;

					return true;
				}

				::close(shfd);
			}

			::close(fd);
		}
	}

	return false;
}

/* For stopping the event loop, we create a shutdown event, monitor it (which causes threads
 * to quit), then wait for all threads to exit as they receive the event. Finally, we delete the
 * shutdown event and then clean up.
 */
/// @todo Force stop with pthread_cancel
void EventLoop::stop() noexcept
{
	std::shared_lock lk(mut);

	EventControl control(efd, shutdown_fd);

	control.stop();
}

void EventLoop::wait()
{
	std::uint_fast32_t val = thread_counter;

	while ( val != 0 )
	{
		thread_counter.wait(val);
		val = thread_counter;
	}
}

/// @todo We should have some way to know if e is deleted. It will be removed from
/// epoll, so that's not a problem, but if the event loop is stopped, we will probably
/// iterate over a list of these sources and try to notify them of the stoppage. We
/// don't want to be doing that with objects that are already deleted
bool EventLoop::subscribe(EventSource* e)
{
	// Add the event to the epoll instance
	std::shared_lock lk(mut);

	EventControl control(efd, shutdown_fd);

	return control.subscribe(e);
}

void EventLoop::epoll_thread_wrapper()
{
	epoll_thread();

	// Clean up if this is the last thread
	/// @todo Relax memory ordering to memory_order_acq_rel
	if ( thread_counter.fetch_sub(1) == 1 )
	{
		close_epoll();

		// Notify any threads sitting on EventLoop::wait
		/// @todo notify_all_at_thread_exit
		thread_counter.notify_all();
	}
}

// Release resources associated with epoll server after all threads have been stopped
/// @todo What if we temporarily set the number of threads to 0, but didn't shut down?
void EventLoop::close_epoll()
{
	std::scoped_lock lk(mut);

	epoll_event event = {};

	::epoll_ctl(efd, EPOLL_CTL_DEL, shutdown_fd, &event);
	::close(shutdown_fd);

	shutdown_fd = -1;

	/// @todo epoll_pwait2 with a time out to see if there were any queued events that need to be
	/// handled. Get them, remove them from epoll, and gracefully cancel the objects

	/// @todo Notify all monitored sources that the eventloop has stopped
	::close(efd);
	efd = -1;
}

EventLoop::result EventLoop::epoll_thread()
{
	EventControl control(efd, shutdown_fd);

	// Buffer to receive epoll events. Since we are multithreaded, we get one and let the other threads
	// get any other events
	epoll_event events[1];

	/// @todo while running_threads <= allowed_threads. Could use a semaphore (or something like
	/// it), do a release and acquire to see if we're still ok
	while ( true )
	{
		/// @todo Maybe epoll_pwait2
		const int n = ::epoll_wait(control.efd, events, 1, -1);

		// Block this thread until an event is available (or error, or spurious wakeup)
		if ( n == 1 )
		{
			if ( auto* const e = static_cast< EventSource* >( events[0].data.ptr ) )
			{
				// Handle the event
				const auto res = e->handle_event(static_cast< event_type >( events[0].events ), control);

				if ( res == EventSource::result::finished )
				{
					epoll_event event = {};

					::epoll_ctl(control.efd, EPOLL_CTL_DEL, e->fd, &event);

					/// @todo Need something to indicate loop is responsible for delete
					/// @todo A different message for "no longer being monitored"
					/// @bug Not every e is owned by EventLoop
					e->about_to_be_deleted();

					/// @bug Non one-shot eventsources may be active in other threads
					delete e;
				}
				else if ( e->event_hint == EventSource::oneshot )
				{
					// We are responsible for re-enabling
					auto flags = e->events_to_monitor;

					// If write is one of the events to listen to, we may not be ready to enable it yet
					if ( (flags& event_type::write) != event_type::none && !( e->set_wait_for_write(false) ) )
					{
						flags ^= event_type::write;
					}

					epoll_event event = { .events = static_cast< std::uint32_t >( flags ) | EPOLLONESHOT, .data = { .ptr = e }};

					/// @todo Failure?
					::epoll_ctl(control.efd, EPOLL_CTL_MOD, e->fd, &event);
				}
			}
			else
			{
				// received a shutdown event
				return normal_exit;
			}
		}
		else if ( n == -1 && errno != EINTR )
		{
			// Fatal error
			return fatal_error;
		}
	}
}

}
