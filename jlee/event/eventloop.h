#ifndef JLEE_EVENT_EVENTLOOP_H
#define JLEE_EVENT_EVENTLOOP_H

#include <shared_mutex>
#include <atomic>

#include "noncopyable.h"

namespace jl
{
class EventSource;

/** @brief Interface for EventSource to accesss EventLoop
 *
 * Some functions in EventLoop (ex., stop and subscribe) incur the cost of
 * locking a shared mutex. Since EventSource almost always makes calls
 * downstream from EventLoop::handle_event, that lock is unnecessary.
 *
 * This object has been introduced to do three things, then:
 *   - Faster calls that don't need to lock the mutex
 *   - Restrict which calls can be made (ex., don't want EventSources calling
 *     EventLoop::start, EventLoop::wait)
 *   - Provides a thread local copy of efd and shutdown_fd to reduce any
 *     contention
 *
 *  @todo Maybe make this a base class of EventLoop
 *  @todo Maybe this acquires the shared_lock and is constructed at the top
 *   of epoll_thread
 */
class EventControl
{
	friend class EventLoop;
public:
	EventControl(int, int);
	void stop() noexcept;
	bool subscribe(EventSource*) noexcept;
private:
	const int efd;

	/// Eventfd used to shut down the epoll threads asynchronously
	const int shutdown_fd;
};

/// @invariant nthread != 0 -> efd != -1
/// @invariant eventfd != -1 -> efd != -1
/// @todo signalfd
/// @todo pipe for unidirectional process communication
/// @todo linux kernel semaphore
/// @todo inotify/fanotify
/// @todo epoll another epoll
/// @todo memfd (?)
/// @todo socketpair for bidirectional thread/process communication
/// @todo stdin, stdout, stderr esp for a child process
/// @todo Support epolls constrained to CCX or core, if possible
class EventLoop
	: NonCopyable
{
public:
	~EventLoop();

	/** @note User must call stop() and wait() before destruction of this object or the destructor
	 *  will call std::terminate.
	 */
	bool start() noexcept;

	/// @todo Perhaps return a future
	/// @todo Maybe this one calls wait, while EventControl::stop doesn't
	void stop() noexcept;

	void wait();

	bool subscribe(EventSource*);
private:
	enum result {normal_exit, fatal_error, unknown_event};

	bool init() noexcept;
	void epoll_thread_wrapper();

	/// @todo Can probably be static now
	result epoll_thread();
	void close_epoll();

	/// Guard efd and shutdown_fd
	mutable std::shared_mutex mut;

	/// File descriptor for epoll instance
	int efd = -1;

	/// Eventfd used to shut down the epoll threads asynchronously
	int shutdown_fd = -1;

	/// @todo atomic_unsigned_lock_free
	std::atomic_uint_fast32_t thread_counter{ 0 };
};
}

#endif // JLEE_EVENT_EVENTLOOP_H
