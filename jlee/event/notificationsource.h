#ifndef JLEE_EVENT_NOTIFICATIONSOURCE_H
#define JLEE_EVENT_NOTIFICATIONSOURCE_H

#include <cstdint>

#include "eventsource.h"

namespace jl
{
class NotificationSource
	: public EventSource
{
public:
	explicit NotificationSource(EventSource::hint, bool close_on_exec = true);

	void notify();
	void notify_all();
	result handle_event(event_type, EventControl&) override;
private:
	void notify_inner(std::uint64_t);
	virtual void triggered(EventControl& control);
};
}

#endif // JLEE_EVENT_NOTIFICATIONSOURCE_H
