#ifndef JLEE_EVENT_NONCOPYABLE_H
#define JLEE_EVENT_NONCOPYABLE_H

class NonCopyable
{
public:
	NonCopyable() = default;

	NonCopyable(const NonCopyable&) = delete;
	NonCopyable(NonCopyable&&)      = delete;

	NonCopyable& operator=(const NonCopyable&) = delete;
	NonCopyable& operator=(NonCopyable&&)      = delete;
};

#endif // JLEE_EVENT_NONCOPYABLE_H
