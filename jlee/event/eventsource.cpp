#include "eventsource.h"

#include <stdexcept>

#include <unistd.h>

namespace jl
{
EventSource::EventSource(
	hint       event_hint_,
	int        fd_,
	event_type events_to_monitor_
) noexcept
	: fd(fd_), events_to_monitor(events_to_monitor_), event_hint(event_hint_), wait_for_write(false)
{
}

EventSource::~EventSource() noexcept
{
	::close(fd);
}

bool EventSource::set_wait_for_write(bool b)
{
	const bool x = wait_for_write;

	wait_for_write = b;
	return x;
}

}
