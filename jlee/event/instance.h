#ifndef JLEE_EVENT_INSTANCE_H
#define JLEE_EVENT_INSTANCE_H

#include "eventloop.h"
#include "eventsources/functionevent.h"

namespace jl
{
namespace details
{
FunctionEvent* default_executor_eventsource();
}

EventLoop& default_application_eventloop();

/// @note if the server is not running, the Executor will do nothing
/// @note The function passed in must not throw (requires noexcept)
/// @todo Avoid copying/moving the function object to the executor if create_execute_event returns null
/// @todo This should really leverage the same interfaces that we would use for a server/socket. No
/// special handling.
template< typename Function >
auto create_executor(Function&& f)
{
	// there is one EventLoop, so one FunctionEvent object
	// When it's created, it has to schedule itself with the EventLoop
	// When the EventLoop stops, it needs to be notified so it can be deleted
	// The FutureEvent, or indeed any Event, must not be in the middle of running when deleted

	return details::default_executor_eventsource()->create_executor( std::forward< Function >(f) );
}

}

#endif // JLEE_EVENT_INSTANCE_H
