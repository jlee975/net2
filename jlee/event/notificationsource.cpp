#include "notificationsource.h"

#include <sys/eventfd.h>

namespace jl
{
/// @bug In the case of FunctionEvent, should be one shot. Not so for the shutdown event
NotificationSource::NotificationSource(
	hint t,
	bool close_on_exec
)
	: EventSource(t, ::eventfd(0, ( close_on_exec ? EFD_CLOEXEC : 0 ) | EFD_NONBLOCK | EFD_SEMAPHORE), event_type::read)
{
}

void NotificationSource::notify()
{
	notify_inner(1);
}

void NotificationSource::notify_all()
{
	notify_inner( UINT64_C(0xfffffffffffffffe) );
}

void NotificationSource::notify_inner(std::uint64_t n)
{
	/// @bug Handle failure
	::eventfd_write(fd, n);
}

NotificationSource::result NotificationSource::handle_event(
	event_type,
	EventControl& control
)
{
	/// @bug Check for event_type::read
	eventfd_t temp = 0;

	/// @todo is it possible to read 0 here? Can multiple threads wake up on the eventfd?
	/// @bug Handle failure
	if ( ::eventfd_read(fd, &temp) == 0 && temp != 0 )
	{
		triggered(control);
	}

	return result::reenable;
}

void NotificationSource::triggered(EventControl& control)
{

}

}
