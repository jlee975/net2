#include "instance.h"

/// @todo Should be removed
#include <sys/epoll.h>

namespace
{
class DefaultApplicationConfiguration
{
public:
	static DefaultApplicationConfiguration& instance()
	{
		static DefaultApplicationConfiguration object;

		return object;
	}

	jl::FunctionEvent ev;
	jl::EventLoop     el;
private:
	DefaultApplicationConfiguration()
	{
		el.start();
		el.subscribe(&ev);
	}

};
}

namespace jl
{
namespace details
{
FunctionEvent* default_executor_eventsource()
{
	return std::addressof(DefaultApplicationConfiguration::instance().ev);
}

}

EventLoop& default_application_eventloop()
{
	return DefaultApplicationConfiguration::instance().el;
}

}
