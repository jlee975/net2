#ifndef JLEE_EVENT_EVENT_TYPE_H
#define JLEE_EVENT_EVENT_TYPE_H

#include <type_traits>
#include <cstdint>
#include <sys/epoll.h>

namespace jl
{
enum class event_type : std::uint32_t
{
	none = 0,
	read = EPOLLIN,
	write = EPOLLOUT,
	error = EPOLLERR,
	hangup = EPOLLHUP,
	rdhup = EPOLLRDHUP,
	edge_triggered = EPOLLET
};

inline event_type operator|(
	event_type l,
	event_type r
)
{
	using X = std::underlying_type_t< event_type >;

	return static_cast< event_type >( static_cast< X >( l ) | static_cast< X >( r ) );
}

inline event_type operator&(
	event_type l,
	event_type r
)
{
	using X = std::underlying_type_t< event_type >;

	return static_cast< event_type >( static_cast< X >( l ) & static_cast< X >( r ) );
}

inline event_type operator^(
	event_type l,
	event_type r
)
{
	using X = std::underlying_type_t< event_type >;

	return static_cast< event_type >( static_cast< X >( l ) ^ static_cast< X >( r ) );
}

inline event_type& operator|=(
	event_type& l,
	event_type  r
)
{
	l = l | r;
	return l;
}

inline event_type& operator&=(
	event_type& l,
	event_type  r
)
{
	l = l & r;
	return l;
}

inline event_type& operator^=(
	event_type& l,
	event_type  r
)
{
	l = l ^ r;
	return l;
}

}

#endif // JLEE_EVENT_EVENT_TYPE_H
