#include "run.h"

#include <cstring>

#include <unistd.h>
#include <sys/mman.h>
#include <sys/wait.h>

namespace
{
// Used because execvp expects char* array (i.e., not const char* )
char* copy_arg(const std::string& s)
{
	const std::size_t n = s.length();

	char* t = new char [n + 1];

	s.copy(t, n);
	t[n] = '\0';

	return t;
}

char** make_argv(
	const std::filesystem::path&      exe,
	const std::vector< std::string >& args
)
{
	const std::size_t n = args.size();

	/// @todo Maybe this should happen before the fork so we can return the error sooner
	char** argv = new char*[( n + 1 ) + 1];

	argv[0] = copy_arg(exe);

	for (std::size_t i = 0; i < n; ++i)
	{
		argv[i + 1] = copy_arg(args[i]);
	}

	argv[n + 1] = nullptr;

	return argv;
}

}

void run(
	const std::filesystem::path&      exe,
	const std::vector< std::string >& args
)
{
	/// @todo Does this mmap get inherited by the execvp below? Any way to ensure it is released?
	void* const pmem_ = ::mmap(nullptr, sizeof( int ), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);

	if ( !pmem_ )
	{
		return;
	}

	int* const pmem = static_cast< int* >( pmem_ );

	*pmem = 0;

	const auto child = ::fork();

	if ( child == 0 )
	{
		// In child
		try
		{
			char** const argv = make_argv(exe, args);

			::execvp(argv[0], argv);

			// Some sort of error
			*pmem = errno;
		}
		catch ( std::bad_alloc& e )
		{
			// couldn't allocate memory
			*pmem = ENOMEM;
		}

		::munmap( pmem, sizeof( int ) );

		_Exit(EXIT_FAILURE);
	}

	// In parent
	if ( child != -1 )
	{
		// Wait for child to finish
		// Wait for the process to exit
		int status = EXIT_FAILURE;

		::waitpid(child, &status, 0);

		/// @todo Use pmem to check the success or failure of the child
	}

	::munmap( pmem, sizeof( int ) );
}
