#ifndef RUN_H
#define RUN_H

#include <vector>
#include <string>
#include <filesystem>

/// @todo Proper process code
void run(const std::filesystem::path& exe, const std::vector< std::string >& args = {});


#endif // RUN_H
